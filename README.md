# Deprecation Notice

No longer maintained. I have finally decided to embrace the tmux (and you should too).

# Description

A harness which provides a mechanism for launching shell commands
in a terminal window by interacting with a named pipe. This 
is useful for creating a remotely controllable terminal window next
to your editor.

# Usage

Terminal 1

`harness /tmp/commandchannel`

Terminal 2

`echo 'ls' > /tmp/commandchannel`

The output of 'ls' should now be displayed in Terminal 1.

Commands are passed into /bin/sh so standard
shell operators can be used to chain commands
together. 

E.G
`echo 'clear;find /|grep 42' > /tmp/commandchannel`

Each time a new command is run the old one is automatically killed.
Thus if you previously executed a long running command like ping you 
can execute a new command without having the old one pollute stdout.

`echo 'clear;gcc test.c -o /tmp/test && /tmp/test' > /tmp/commandchannel`

Pairs nicely with some editor glue.
