all:
	-rm -rf bin 2>&1 > /dev/null
	mkdir bin
	gcc -pthread harness.c -o bin/harness
install:
	install -m 755 bin/harness /usr/bin
